/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author apayan
 *
 */
public class Fahrenheit extends Temperature
{
public Fahrenheit(float t)
{
super(t);
}
public String toString()
{
// TODO: Complete this method
return String.valueOf(value);
}
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	return new Celsius((float)((value-32)/1.8));
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return new Fahrenheit(value);
}
}
