/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author apayan
 *
 */
public class Celsius extends Temperature
{
public Celsius(float t)
{
super(t);
}
public String toString()
{
// TODO: Complete this method
	return String.valueOf(value);
}
@Override
public Temperature toCelsius(){
	// TODO Auto-generated method stub
	return new Celsius(value);
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	return new Fahrenheit((float)(value*1.8)+32);
}
}